﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JustDoIt.DB.Queries.UserQueries;

namespace UnitTests.DatabaseTests
{
    [TestClass]
    public class UserQueriesTest
    {
        private const int Id = 1065;
        private const string Name = "test123";
        private const string Password = "password123";
        private const string TableName = "user";
        private const string SelectUserWithName = "Select * from [dbo].[" + TableName + "] where name = '" + Name + "'";
        private const string SelectUserWithNameAndPassword = "Select * from [dbo].[" + TableName + "] where password = '" + Password + "' and name = '" + Name + "'";
        private static readonly string SelectUserById = "Select * from [dbo].[" + TableName + "] where id = '" + Id + "'";

        [TestMethod]
        public void FindUserWithNameTest()
        {
            var query = new FindUserWithName(Name);
            Assert.AreEqual(query.GenerateQuery(TableName), SelectUserWithName);
        }

        [TestMethod]
        public void GetUserById()
        {
            var query = new GetUserById(Id);
            Assert.AreEqual(query.GenerateQuery(TableName), SelectUserById);
        }

        [TestMethod]
        public void GetUserByName()
        {
            var query = new GetUserByName(Name, Password);
            Assert.AreEqual(query.GenerateQuery(TableName), SelectUserWithNameAndPassword);
        }
    }
}
