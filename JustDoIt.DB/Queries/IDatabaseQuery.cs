﻿namespace JustDoIt.DB.Queries
{
    public interface IDatabaseQuery<TItem> where TItem : class
    {
        string GenerateQuery(string tableName);
    }
}
