﻿using System;
using JustDoIt.DB.Entity;

namespace JustDoIt.DB.Queries.UserQueries
{
    public class GetUserById : IDatabaseQuery<User>
    {
        private const string SelectUserById = "Select * from [dbo].[{0}] where id = '{1}'";
        private readonly int _id;

        public GetUserById(int id)
        {
            _id = id;
        }

        public string GenerateQuery(string tableName)
        {
            return String.Format(SelectUserById, tableName, _id);
        }
    }
}
