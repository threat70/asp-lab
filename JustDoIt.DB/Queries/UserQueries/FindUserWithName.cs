﻿using System;
using JustDoIt.DB.Entity;

namespace JustDoIt.DB.Queries.UserQueries
{
    public class FindUserWithName : IDatabaseQuery<User>
    {
        private const string SelectUserWithName = "Select * from [dbo].[{0}] where name = '{1}'";
        private readonly string _name;

        public FindUserWithName(string name)
        {
            _name = name;
        }

        public string GenerateQuery(string tableName)
        {
            return String.Format(SelectUserWithName, tableName, _name);
        }
    }
}
