﻿using System;
using JustDoIt.DB.Entity;

namespace JustDoIt.DB.Queries.UserQueries
{
    public class GetUserByName : IDatabaseQuery<User>
    {
        private const string SelectUserWithNameAndPassword = "Select * from [dbo].[{0}] where password = '{1}' and name = '{2}'";
        private readonly string _name;
        private readonly string _password;

        public GetUserByName(string name, string password)
        {
            _name = name;
            _password = password;
        }

        public string GenerateQuery(string tableName)
        {
            return String.Format(SelectUserWithNameAndPassword, tableName, _password, _name);
        }
    }
}
