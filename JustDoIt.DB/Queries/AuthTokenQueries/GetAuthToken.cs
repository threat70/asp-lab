﻿using System;
using JustDoIt.DB.Entity;

namespace JustDoIt.DB.Queries.AuthTokenQueries
{
    public class GetAuthToken : IDatabaseQuery<AuthToken>
    {
        private readonly string _token;
        private const string SelectToken = "Select * from [dbo].[{0}] where auth_token = '{1}'";

        public GetAuthToken(string token)
        {
            _token = token;
        }

        public string GenerateQuery(string tableName)
        {
            return String.Format(SelectToken, tableName, _token);
        }
    }
}
