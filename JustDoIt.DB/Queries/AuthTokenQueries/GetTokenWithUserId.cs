﻿using System;
using JustDoIt.DB.Entity;

namespace JustDoIt.DB.Queries.AuthTokenQueries
{
    public class GetTokenWithUserId : IDatabaseQuery<AuthToken>
    {
        private readonly int _id;
        private const string SelectTokenForUser = "Select * from [dbo].[{0}] where user_id = '{1}'";

        public GetTokenWithUserId(int id)
        {
            _id = id;
        }

        public string GenerateQuery(string tableName)
        {
            return String.Format(SelectTokenForUser, tableName, _id);
        }
    }
}
