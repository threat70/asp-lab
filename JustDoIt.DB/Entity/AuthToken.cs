﻿using JustDoIt.DB.Attributes;

namespace JustDoIt.DB.Entity
{
    [TableName("UserTokens")]
    public class AuthToken
    {
        [PropertyName("Id", true)]
        public int Id { get; set; }
        [PropertyName("user_id")]
        public int UserId { get; set; }
        [PropertyName("auth_token")]
        public string Value { get; set; }
    }
}
