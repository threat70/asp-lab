﻿using System;
using JustDoIt.DB.Attributes;

namespace JustDoIt.DB.Entity
{
    [TableName("User")]
    public class User
    {
        [PropertyName("Id", true)]
        public int Id { get; set; }

        [PropertyName("name")] 
        public String Name { get; set; }

        [PropertyName("password")]
        public String Password { get; set; }

        [PropertyName("first_name")]
        public String FirstName { get; set; }

        [PropertyName("last_name")]
        public String LastName { get; set; }

        //[FieldName("team_id")]
        public int TeamId { get; set; }

        //[FieldName("team_access_type")] 
        public String TeamAccessType { get; set; }
    }
}