﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace JustDoIt.DB
{
    public class DatabaseHelper : IDatabaseHelper
    {
        private readonly string _connectionString;

        public DatabaseHelper()
        {
            _connectionString = "Data Source=(localdb)\\Projects;Initial Catalog=JustDb;Integrated Security=True;Connect Timeout=30";
        }

        public SqlConnection CreateSqlConnection()
        {
            return new SqlConnection(_connectionString);
        }

        public int ExecuteQueries(SqlCommand[] sqlCommands)
        {
            if (sqlCommands.Length == 0) return 0;

            var affectedRows = 0;

            using (var con = CreateSqlConnection())
            {
                con.Open();
                var transaction = con.BeginTransaction();
                foreach (var query in sqlCommands)
                {
                    query.Connection = con;
                    query.Transaction = transaction;
                    affectedRows += query.ExecuteNonQuery();
                    // Warning!! dispose
                    query.Dispose();
                }
                transaction.Commit();
                transaction.Dispose();
            }

            return affectedRows;
        }

        public DataTable GetDataTableWithQuery(string query)
        {
            var dataTable = new DataTable();
            using (var con = CreateSqlConnection())
            {
                con.Open();

                using (var sqlCommand = new SqlCommand(query, con))
                {
                    using (var dataReader = sqlCommand.ExecuteReader())
                    {
                        dataTable.Load(dataReader);   
                    }
                }
            }
            return dataTable;
        }
    }
}
