﻿using System.Collections.Generic;
using JustDoIt.DB.Queries;

namespace JustDoIt.DB
{
    public interface IAdapter<TItem> where TItem : class
    {
        void Insert(TItem entity);
        void UpdateByAutoIncrementValue(TItem entity);
        List<TItem> ToList();
        List<TItem> GetItemsByQuery(IDatabaseQuery<TItem> query);
        TItem GetItemByQuery(IDatabaseQuery<TItem> query);
    }
}
