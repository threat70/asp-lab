﻿using System;

namespace JustDoIt.DB.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class PropertyNameAttribute : Attribute
    {
        public string Name;
        public bool AutoIncrement;

        public PropertyNameAttribute(string name, bool autoIncrement = false)
        {
            Name = name;
            AutoIncrement = autoIncrement;
        }
    }
}
