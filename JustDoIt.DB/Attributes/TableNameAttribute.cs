﻿using System;

namespace JustDoIt.DB.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class TableNameAttribute : Attribute
    {
        public string Name;

        public TableNameAttribute(string name)
        {
            Name = name;
        }
    }
}
