﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace JustDoIt.DB
{
    public class DatabaseContext : IQuerysContainer, IDatabaseContext
    {
        private readonly List<SqlCommand> _queryList;
        private readonly IDatabaseHelper _databaseHelper;  

        public DatabaseContext(IDatabaseHelper databaseHelper)
        {
            _databaseHelper = databaseHelper;
            _queryList = new List<SqlCommand>();
        }

        public void SubmitChanges()
        {
            lock (_queryList)
            {
                _databaseHelper.ExecuteQueries(_queryList.ToArray());
                _queryList.Clear();
            }
        }

        public IAdapter<TItem> GetAdapter<TItem>() where TItem : class
        {
            return new Adapter<TItem>(this, _databaseHelper);
        }

        public void AddToQueue(SqlCommand sql)
        {
            lock (_queryList)
            {
                _queryList.Add(sql);   
            }
        }
    }
}