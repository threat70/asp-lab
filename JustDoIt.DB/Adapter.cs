﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using JustDoIt.DB.Attributes;
using JustDoIt.DB.Queries;

namespace JustDoIt.DB
{
    public class Adapter<TItem> : IAdapter<TItem> where TItem : class
    {
        private const string InsertQuery = "INSERT INTO [dbo].[{0}] ({1}) VALUES ({2})";
        private const string UpdateQuery = "UPDATE [dbo].[{0}] SET {1} WHERE {2}={3}";
        private const string SelectAll = "SELECT * FROM [dbo].[{0}]";
        private const string Where = " WHERE ";
        private const string And = " AND ";
        private readonly IQuerysContainer _querysContainer;
        private readonly IDatabaseHelper _databaseHelper;
        protected readonly string TableName;
        private readonly PropertyInfo[] _propertiesList;

        public Adapter(IQuerysContainer querysContainer, IDatabaseHelper databaseHelper)
        {
            _querysContainer = querysContainer;
            _databaseHelper = databaseHelper;
            TableName = typeof(TItem).GetCustomAttribute<TableNameAttribute>(false).Name;
            _propertiesList = typeof (TItem).GetProperties();
        }

        protected SqlConnection CreateSqlConnection()
        {
            return _databaseHelper.CreateSqlConnection();
        }

        protected void AddToQueue(SqlCommand sqlCommand)
        {
            _querysContainer.AddToQueue(sqlCommand);
        }

        protected void AddToQueue(string sqlQuery)
        {
            AddToQueue(new SqlCommand(sqlQuery));
        }

        public void Insert(TItem entity)
        {
            var valueNames = new List<string>();
            var values = new List<string>();
            foreach (var propertyInfo in _propertiesList)
            {
                var propertyName = propertyInfo.GetCustomAttribute<PropertyNameAttribute>(false);

                // Skip if property setted by auto increment
                if (propertyName == null || propertyName.AutoIncrement) continue;

                var value = propertyInfo.GetValue(entity);
                if (value == null) continue;
                valueNames.Add(propertyName.Name);
                values.Add("'" + value + "'");
            }

            if (values.Count <= 0) return;

            AddToQueue(String.Format(InsertQuery,
                TableName,
                String.Join(",", valueNames),
                String.Join(",", values)));
        }

        public void UpdateByAutoIncrementValue(TItem entity)
        {
            var values = new List<string>();
            string autoIncrementName = null;
            var autoIncrementValue = 0;
            foreach (var propertyInfo in _propertiesList)
            {
                var propertyName = propertyInfo.GetCustomAttribute<PropertyNameAttribute>(false);

                if (propertyName == null) continue;

                var value = propertyInfo.GetValue(entity);
                if (value == null) continue;

                if (propertyName.AutoIncrement)
                {
                    autoIncrementName = propertyName.Name;
                    autoIncrementValue = (int) value;
                    continue;
                }

                values.Add(propertyName.Name + "='" + value + "'");
            }

            if (autoIncrementName == null || autoIncrementValue <= 0) 
                throw new NoNullAllowedException("Autoincrement cannot be null on update");

            if (values.Count <= 0) return;

            AddToQueue(String.Format(UpdateQuery,
                TableName,
                String.Join(", ", values),
                autoIncrementName,
                autoIncrementValue));
        }

        public List<TItem> ToList()
        {
            return GetItemsWithQuery(GenerateFindQuery(null));
        }

        public List<TItem> GetItemsByQuery(IDatabaseQuery<TItem> query)
        {
            return GetItemsWithQuery(query.GenerateQuery(TableName));
        }

        public TItem GetItemByQuery(IDatabaseQuery<TItem> query)
        {
            var list = GetItemsWithQuery(query.GenerateQuery(TableName));
            return ((list.Count > 0) ? list.First() : null);
        }

        private List<TItem> GetItemsWithQuery(string query)
        {
            var result = new List<TItem>();
            var resultTable = _databaseHelper.GetDataTableWithQuery(query);
            foreach (DataRow tableRow in resultTable.Rows)
            {
                var item = Activator.CreateInstance<TItem>();

                foreach (var propertyInfo in _propertiesList)
                {
                    var propertyName = propertyInfo.GetCustomAttribute<PropertyNameAttribute>(false);
                    if (propertyName == null) continue;

                    var value = tableRow[propertyName.Name];
                    propertyInfo.SetValue(item, value);
                }
                result.Add(item);
            }
            return result;
        }

        private string GenerateFindQuery(Dictionary<string, object> parameters)
        {
            var result = String.Format(SelectAll, TableName);
            if (parameters == null || parameters.Count <= 0) return result;

            result += Where + GenerateValues(parameters);

            return result;
        }

        private static string GenerateValues(Dictionary<string, object> parameters)
        {
            var result = "";

            var first = true;
            foreach (var key in parameters.Keys)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    result += And;
                }
                result += key + "='" + parameters[key] + "'";
            }
            return result;
        }
    }
}