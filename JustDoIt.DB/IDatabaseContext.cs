﻿namespace JustDoIt.DB
{
    public interface IDatabaseContext
    {
        void SubmitChanges();

        IAdapter<TItem> GetAdapter<TItem>() where TItem : class;
    }
}
