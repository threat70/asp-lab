﻿using System.Data;
using System.Data.SqlClient;

namespace JustDoIt.DB
{
    public interface IDatabaseHelper
    {
        SqlConnection CreateSqlConnection();

        int ExecuteQueries(SqlCommand[] sqlCommands);

        DataTable GetDataTableWithQuery(string query);
    }
}
