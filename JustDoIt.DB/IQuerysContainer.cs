﻿using System.Data.SqlClient;

namespace JustDoIt.DB
{
    public interface IQuerysContainer
    {
        void AddToQueue(SqlCommand sql);
    }
}
