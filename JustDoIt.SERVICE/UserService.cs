﻿using JustDoIt.DB;
using JustDoIt.DB.Entity;
using JustDoIt.DB.Queries.UserQueries;

namespace JustDoIt.SERVICE
{
    public class UserService : IUserService
    {
        private readonly IDatabaseContext _dbContext;

        public UserService(IDatabaseContext databaseContext)
        {
            _dbContext = databaseContext;
        }

        public CreateUserResult CreateUser(User user)
        {
            if (IsUsedUserName(user.Name))
                return CreateUserResult.NickNameUsed;

            _dbContext.GetAdapter<User>().Insert(user);
            _dbContext.SubmitChanges();

            return CreateUserResult.Success;
        }

        public User GetUser(string name, string password)
        {
            return _dbContext.GetAdapter<User>().GetItemByQuery(new GetUserByName(name, password));
        }

        private bool IsUsedUserName(string name)
        {
            var adapter = _dbContext.GetAdapter<User>();
            var findUserWithName = new FindUserWithName(name);

            return adapter.GetItemByQuery(findUserWithName) != null;
        }
    }
}
