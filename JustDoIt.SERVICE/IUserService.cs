﻿using JustDoIt.DB.Entity;

namespace JustDoIt.SERVICE
{
    public enum CreateUserResult
    {
        NickNameUsed,
        Success
    }

    public interface IUserService
    {
        CreateUserResult CreateUser(User user);
        User GetUser(string name, string password);
    }
}
