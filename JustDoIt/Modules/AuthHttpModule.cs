﻿using System;
using System.Web;
using JustDoIt.Auth;
using JustDoIt.Windsor;

namespace JustDoIt.Modules
{
    public class AuthHttpModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += Authenticate;
        }

        private static void Authenticate(Object source, EventArgs e)
        {
            var app = (MvcApplication)source;
            var context = app.Context;
            var authentication = IocContainer.Resolve<IAuthentication>();

            authentication.HttpContext = context;

            context.User = authentication.CurrentUser;
        }

        public void Dispose()
        {
        }
    }
}