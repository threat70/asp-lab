﻿using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace JustDoIt.Windsor
{
    public static class IocContainer
    {
        private static IWindsorContainer _container;

        public static void Setup()
        {
            _container = new WindsorContainer().Install(FromAssembly.This());

            var controllerFactory = new WindsorControllerFactory(_container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public static T Resolve<T>(string key)
        {
            return _container.Resolve<T>(key);
        }
    }
}