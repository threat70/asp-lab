﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using JustDoIt.Auth;

namespace JustDoIt.Windsor
{
    public class ControllerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                // Register all controllers in assembly. 
                Classes
                    .FromThisAssembly()
                    .BasedOn<IController>()
                    .Configure(
                        reg =>
                            reg.Named(reg.Implementation.Name.ToLowerInvariant()))
                    .LifestylePerWebRequest(),
                Component
                    .For<IAuthentication>()
                    .ImplementedBy<Authentication>()
                    .LifestylePerWebRequest()
            );
        }
    }
}