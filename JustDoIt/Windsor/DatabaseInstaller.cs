﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using JustDoIt.DB;

namespace JustDoIt.Windsor
{
    public class DatabaseInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IDatabaseContext>()
                    .ImplementedBy<DatabaseContext>(),
                Component
                    .For<IDatabaseHelper>()
                    .ImplementedBy<DatabaseHelper>()
                    .LifestyleSingleton()
            );
        }
    }
}