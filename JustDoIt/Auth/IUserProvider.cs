﻿using JustDoIt.DB.Entity;

namespace JustDoIt.Auth
{
    public interface IUserProvider
    {
        User User { get; set; }
    }
}
