﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using JustDoIt.DB;
using JustDoIt.DB.Entity;

namespace JustDoIt.Auth
{
    public class Authentication : IAuthentication
    {
        private const string CookieName = "auth_data";

        private IPrincipal _currentUser;
        private readonly IDatabaseContext _databaseContext;

        public Authentication(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public IPrincipal CurrentUser
        {
            get
            {
                if (_currentUser != null && _currentUser.Identity.IsAuthenticated) return _currentUser;

                try
                {
                    var authCookie = HttpContext.Request.Cookies.Get(CookieName);
                    if (authCookie != null && !string.IsNullOrEmpty(authCookie.Value))
                    {
                        var ticket = FormsAuthentication.Decrypt(authCookie.Value);

                        if (ticket != null)
                        {
                            _currentUser = new UserProvider(ticket.Name, _databaseContext);

                            return _currentUser;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // ignored
                }
                _currentUser = new UserProvider(null, null);
                return _currentUser;
            }
        }

        public HttpContext HttpContext { get; set; }

        public void LogIn(User user)
        {
            HttpContext.Response.Cookies.Set(CreateCookie(user.Name));
        }

        public void LogOut()
        {
            var httpCookie = HttpContext.Response.Cookies[CookieName];
            if (httpCookie != null)
            {
                httpCookie.Value = string.Empty;
            }
            _currentUser = null;
        }

        private static HttpCookie CreateCookie(string userName)
        {
            var ticket = new FormsAuthenticationTicket(
                  1,
                  userName,
                  DateTime.Now,
                  DateTime.Now.Add(FormsAuthentication.Timeout),
                  false,
                  string.Empty,
                  FormsAuthentication.FormsCookiePath);

            var encTicket = FormsAuthentication.Encrypt(ticket);

            return new HttpCookie(CookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
        }
    }
}