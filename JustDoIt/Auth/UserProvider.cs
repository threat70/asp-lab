﻿using System.Security.Principal;
using JustDoIt.DB;

namespace JustDoIt.Auth
{
    /// <summary>
    /// Реализация интерфейса Principal
    /// </summary>
    public class UserProvider : IPrincipal
    {
        private UserIndentity UserIdentity { get; set; }

        #region IPrincipal Members

        /// <summary>
        ///     Идентификатор пользователя
        /// </summary>
        public IIdentity Identity
        {
            get
            {
                return UserIdentity;
            }
        }

        /// <summary>
        ///     Находится в данной роли или нет
        /// </summary>
        /// <param name="role">имя роли</param>
        /// <returns></returns>
        public bool IsInRole(string role)
        {
            return UserIdentity.User != null; // Роли не поддерживаются
        }

        #endregion

        /// <summary>
        ///     Инициализация по имени
        /// </summary>
        /// <param name="name">Имя пользователя</param>
        /// <param name="dbContext">Контекст для работы с базой данных</param>
        public UserProvider(string name, IDatabaseContext dbContext)
        {
            UserIdentity = new UserIndentity(name, dbContext);
        }


        /// <summary>
        /// Имя пользователя
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return UserIdentity.Name;
        }
    }
}
