﻿using System.Security.Principal;
using System.Web;
using JustDoIt.DB.Entity;

namespace JustDoIt.Auth
{
    public interface IAuthentication
    {
        IPrincipal CurrentUser { get; }
        HttpContext HttpContext { get; set; }
        void LogIn(User user);
        void LogOut();
    }
}
