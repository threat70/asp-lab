﻿using System.Security.Principal;
using JustDoIt.DB;
using JustDoIt.DB.Entity;
using JustDoIt.DB.Queries.UserQueries;

namespace JustDoIt.Auth
{
    public class UserIndentity : IIdentity, IUserProvider
    {
        /// <summary>
        ///     Текщий пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        ///     Тип класса для пользователя
        /// </summary>
        public string AuthenticationType
        {
            get
            {
                return typeof(User).ToString();
            }
        }

        /// <summary>
        ///     Авторизован или нет
        /// </summary>
        public bool IsAuthenticated
        {
            get
            {
                return User != null;
            }
        }

        /// <summary>
        ///     Имя пользователя (уникальное)
        /// </summary>
        public string Name
        {
            get
            {
                if (User != null)
                {
                    return User.Name;
                }
                //иначе аноним
                return "anonym";
            }
        }

        /// <summary>
        ///     Инициализация по имени
        /// </summary>
        /// <param name="name">Имя пользователя</param>
        /// <param name="dbContext">Контекст для работы с базой данных</param>
        public UserIndentity(string name, IDatabaseContext dbContext)
        {
            if (string.IsNullOrEmpty(name)) return;

            var adapter = dbContext.GetAdapter<User>();
            User = adapter.GetItemByQuery(new FindUserWithName(name));
        }
    }
}
