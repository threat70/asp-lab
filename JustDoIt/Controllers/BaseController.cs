﻿using System.Web.Mvc;
using JustDoIt.Auth;
using JustDoIt.DB.Entity;
using JustDoIt.Windsor;

namespace JustDoIt.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly IAuthentication Authentication = IocContainer.Resolve<IAuthentication>();

        public User CurrentUser
        {
            get
            {
                return ((IUserProvider)HttpContext.User.Identity).User;
            }
        }
    }
}
