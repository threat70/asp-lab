﻿using System.Web.Mvc;
using JustDoIt.Mappers;

namespace JustDoIt.Controllers
{
    public class IndexController : BaseController
    {
        public ActionResult Index()
        {
            return CurrentUser != null ? View(UserMapper.Map(CurrentUser)) : View();
        }
    }
}
