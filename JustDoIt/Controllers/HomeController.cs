﻿using System.Web.Mvc;
using JustDoIt.ViewModels;

namespace JustDoIt.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index(UserViewModel model)
        {
            return View(model);
        }

        public ActionResult LogOut()
        {
            Authentication.LogOut();

            return RedirectToAction("Index", "Login");
        }

    }
}
