﻿using System.Web.Mvc;

namespace JustDoIt.Controllers
{
    public class WelcomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
