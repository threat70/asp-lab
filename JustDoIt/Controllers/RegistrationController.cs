﻿using System.Web.Mvc;
using JustDoIt.Mappers;
using JustDoIt.SERVICE;
using JustDoIt.ViewModels;

namespace JustDoIt.Controllers
{
    public class RegistrationController : BaseController
    {
        private readonly IUserService _userService;

        public RegistrationController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(UserViewModel userViewModel)
        {
            var user = UserMapper.Map(userViewModel);
            var result = _userService.CreateUser(user);

            switch (result)
            {
                case CreateUserResult.NickNameUsed:
                    return View(userViewModel);
                case CreateUserResult.Success:
                    Authentication.LogIn(user);
                    break;
            }

            return RedirectToAction("Index", "Index");
        }

    }
}
