﻿using System.Web.Mvc;
using JustDoIt.SERVICE;
using JustDoIt.ViewModels;

namespace JustDoIt.Controllers
{
    public class LoginController : BaseController
    {
        private readonly IUserService _userService;

        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(UserViewModel userViewModel)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.GetUser(userViewModel.Name, userViewModel.Password);

                if (user != null)
                {
                    Authentication.LogIn(user);
                    return RedirectToAction("Index", "Index");
                }

                ModelState["Password"].Errors.Add("Данные введены неверно");
            }
            return View(userViewModel);
        }
    }
}