﻿using JustDoIt.DB.Entity;
using JustDoIt.ViewModels;

namespace JustDoIt.Mappers
{
    public class UserMapper //: IMapper<User, UserViewModel>
    {
        public static User Map(UserViewModel source)
        {
            return new User()
            {
                FirstName = source.FirstName,
                LastName = source.LastName,
                Name = source.Name,
                Password = source.Password
            };
        }

        public static UserViewModel Map(User source)
        {
            return new UserViewModel()
            {
                FirstName = source.FirstName,
                LastName = source.LastName,
                Name = source.Name,
                Password = source.Password,
                Authorized = true
            };
        }
    }
}