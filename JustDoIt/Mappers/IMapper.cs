﻿namespace JustDoIt.Mappers
{
    public interface IMapper<TFirstObject, TSecondObject>
    {
        TSecondObject Map(TFirstObject source);

        TFirstObject Map(TSecondObject source);
    }
}
